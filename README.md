# DimensionalCalculations
Calculations of values with dimension

## Overview
### Unit names

**Amount of substance**
| Unit | String, EN | String, RU |
| ---- | ---------- | ---------- |
| Mole | `mol`      | `моль`     |

**Current**
| Unit   | String, EN | String, RU |
| ------ | ---------- | ---------- |
| Ampere | `A`        | `А`        |

**Length**
| Unit              | String, EN | String, RU |
| ----------------- | ---------- | ---------- |
| Meter             | `m`        | `м`        |
| Mile              | `mi`       | `миля`     |
| Light year        | `ly`       | `св.год`   |
| Astronomical unit | `au`, `AU` | `а.е.`     |
| Parsec            | `pc`       | `пк`       |
| Angstrem          | `Å`        |            |

**Luminous intensity**
| Unit    | String, EN | String, RU |
| ------- | ---------- | ---------- |
| Candela | `cd`       | `кд`       |

**Mass**
| Unit    | String, EN | String, RU |
| ------- | ---------- | ---------- |
| Carat   | `ct`       | `кар`      |
| Gram    | `g`        | `г`        |
| Pound   | `lb`       |            |
| Tonne   | `t`        | `т`        |

**Temperature**
| Unit       | String, EN | String, RU |
| ---------- | ---------- | ---------- |
| Celsius    | `°C`       | `°С`       |
| Kelvin     | `K`        | `К`        |
| Fahrenheit | `°F`       |            |

**Time**
| Unit    | String, EN | String, RU |
| ------- | ---------- | ---------- |
| Second  | `s`        | `с`        |

**Complex units**
Area
| Unit | String, EN | String, RU |
| ---- | ---------- | ---------- |
| Are  | `a`        | `а`, `ар`  |
| Acre | `ac`       | `акр`      |

Volume
| Unit   | String, EN | String, RU |
| ------ | ---------- | ---------- |
| Litre  | `l`, `L`   | `л`        |
| Pint   | `pint`     | `пинта`    |
| Barrel | `bbls`     | `баррель`  |

Force
| Unit   | String, EN | String, RU |
| ------ | ---------- | ---------- |
| Newton | `N`        | `Н`        |
| Dyne   | `dyn`      | `дина`     |

Power
| Unit          | String, EN | String, RU |
| ------------- | ---------- | ---------- |
| Watt          | `W`        | `Вт`       |
| Horse power   | `hp`       | `лс`       |

Pressure
| Unit                 | String, EN      | String, RU |
| -------------------- | --------------- | ---------- |
| Pascal               | `Pa`            | `Па`       |
| Bar                  | `bar`           | `бар`      |
| Standard Atmosphere  | `atm`           | `атм`      |
| Torr                 | `Torr`          |            |
| mmHg                 | `mmHg`, `mm.Hg` | `мм.рт.ст` |

Energy
| Unit         | String, EN | String, RU |
| ------------ | ---------- | ---------- |
| Joule        | `J`        | `Дж`       |
| Erg          | `erg`      | `эрг`      |
| Electronvolt | `eV`       | `эВ`       |

Other electricity units
| Unit    | String, EN | String, RU |
| ------- | ---------- | ---------- |
| Volt    | `V`        | `В`        |
| Ohm     | `Ohm`, `Ω` | `Ом`       |
| Coulomb | `C`        | `Кл`       |
| Farad   | `F`        | `Ф`        |

### Metric prefixes
Sometimes it is needed to operate with units with metric prefixes.
The app supports shjwed below multiple and submultiple prefixes.

**Multiple prefixes**

| Prefix | Value | String, EN | String, RU |
| ------ | ----- | ---------- | ---------- |
| Deca   | 10^1  | `da`       | `да`       |
| Hecto  | 10^2  | `h`        | `г`        |
| Kilo   | 10^3  | `k`        | `к`        |
| Mega   | 10^6  | `M`        | `М`        |
| Giga   | 10^9  | `G`        | `Г`        |
| Tera   | 10^12 | `T`        | `Т`        |
| Peta   | 10^15 | `P`        | `П`        |
| Exa    | 10^18 | `E`        | `Э`        |
| Zetta  | 10^21 | `Z`        | `З`        |
| Yotta  | 10^24 | `Y`        | `И`        |

**Submultiple prefixes**

| Prefix | Value  | String, EN | String, RU |
| ------ | ------ | ---------- | ---------- |
| Deci   | 10^-1  | `d`        | `д`        |
| Centi  | 10^-2  | `c`        | `с`        |
| Milli  | 10^-3  | `m`        | `м`        |
| Micro  | 10^-6  | `u`, `μ`   | `мк`       |
| Nano   | 10^-9  | `n`        | `н`        |
| Pico   | 10^-12 | `p`        | `п`        |
| Femto  | 10^-15 | `f`        | `ф`        |
| Atto   | 10^-18 | `a`        | `а`        |
| Zepto  | 10^-21 | `z`        | `з`        |
| Yocto  | 10^-24 | `y`        | `и`        |

## Examples

**Converting to international system of units**

From gram to kilogram:

Input:
`1005 g = `

Output:
`1005 g = 1.005 kg`

**Simplifying units**

Input:
`15 kg * m / s^2 = `

Output:
`15 kg * m / s^2 = 15 N`

**Calculating new units**

Input:
`15 kg * m / s^2 * 2 m = `

Output:
`15 kg * m / s^2 * 2 m = 30 J`
